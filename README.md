# Pipeline CI avec Flake8 et Python

Article original : [Lint Python avec Flake8 sur Gitlab-CI](https://medium.com/@jordan.assouline/lint-python-avec-flake8-sur-gitlab-ci-ab6f88ef2cee)
Ce dépôt présente un fichier **.gitlab-ci.yml** permettant de lancer un Job de vérification automatique de la conformité du code du fichier **password_check.py** au **Flake8** (dans sa configuration par défaut).

[![pipeline status](https://gitlab.com/jassouline/pipeline-ci-avec-flake8-et-python/badges/main/pipeline.svg)](https://gitlab.com/jassouline/pipeline-ci-avec-flake8-et-python/-/commits/main)

# Fichiers
## password_check.py
Simple script Python3, permettant de vérifier si le mot de passe saisi par l'utilisateur est contenu dans la liste dans 500 mots de passe les plus utilisés (https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/500-worst-passwords.txt) puis dans celle des 10 000 mots de passe les plus utilisés (https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10k-most-common.txt).

Pour lancer le script :

    python3 password_check.py
    
## .gitlab-ci.yml
Fichier décrivant la pipeline d'intégration continue à exécuter grâce à Gitlab-CI :

 - Création d'un **stage** appelé *lint*
 - Création d'un **job** appelé *flake8_code*
	 - Appartenance du job au stage lint
	 - Utilisation de l'image Docker : *python*
	 - Installation de Flake8 via l'outil *pip*
	 - Exécution de *Flake8* sur le fichier *password_check.py*
	 - Autorisation d'échec pour le Job

```yaml
stages:
- lint

flake8_code:
  stage: lint
  image: python
  
  before_script:
  - pip install flake8
  
  script:
  - flake8 password_check.py
  
  allow_failure: true
```